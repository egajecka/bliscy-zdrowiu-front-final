$(function() {
	var aboutUsContainer = $('#about-us-list');
	aboutUsContainer.find('p').hide();
	aboutUsContainer.find('h2').click(function() {
		$(this).toggleClass('expanded');
		$(this).siblings('p').toggle();
	});
});